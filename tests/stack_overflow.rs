#![no_std]
#![no_main]

use core::panic::PanicInfo;
use sanso::panic::*;
use sanso::*;

#[no_mangle]
pub extern "C" fn _start() {
    init();
    stack_overflow(LinkedList {
        data: 0,
        next: None,
    });
    serial_println!("[stack overflow test did not panic]");
    abort(ExitCode::Failure)
}

#[panic_handler]
pub fn panic(_info: &PanicInfo) -> ! {
    serial_println!("[ok]");
    abort(ExitCode::Success)
}

#[derive(Debug)]
pub struct LinkedList<'a> {
    data: u64,
    next: Option<&'a LinkedList<'a>>,
}

pub fn stack_overflow(list: LinkedList) {
    if list.data >= u64::MAX {
        // To avoid any possible optimization of this loop
        println!("{:?}", list)
    }
    stack_overflow(LinkedList {
        data: list.data + 1,
        next: Some(&list),
    })
}
