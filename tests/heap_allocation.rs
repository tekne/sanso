#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(sanso::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use alloc::boxed::Box;
use alloc::string::ToString;
use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use sanso::*;
use x86_64::{instructions::hlt, VirtAddr};

entry_point!(main);

fn main(boot_info: &'static BootInfo) -> ! {
    sanso::init();
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator =
        unsafe { memory::BootInfoFrameAllocator::init(&boot_info.memory_map) };
    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");

    test_main();
    loop {
        hlt()
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    sanso::testing::test_panic_handler(info)
}

#[test_case]
fn simple_allocation() {
    let heap_value_1 = Box::new(41);
    let heap_value_2 = Box::new(13);
    assert_ne!(&*heap_value_1 as *const _, &*heap_value_2 as *const _);
    assert_eq!(*heap_value_1, 41);
    assert_eq!(*heap_value_2, 13);
}

#[test_case]
fn many_allocations() {
    for i in 0..100000 {
        i.to_string();
    }
}
