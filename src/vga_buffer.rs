use core::fmt::{self, Write};
use lazy_static::lazy_static;
use spin::Mutex;
use volatile::Volatile;

/// VGA colors
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(u8)]
#[allow(dead_code)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

/// A color code, consisting of a 4-bit background color and a 4-bit foreground color
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct ColorCode(u8);

impl ColorCode {
    pub const fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

/// A VGA buffer character, consisting of an 8-bit code page 437 codepoint and a color code
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(C)]
pub struct ScreenChar {
    code: u8,
    color_code: ColorCode,
}

/// The VGA buffer height
const BUFFER_HEIGHT: usize = 25;
/// The VGA buffer width
const BUFFER_WIDTH: usize = 80;

/// A VGA buffer
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct Buffer {
    chars: [ScreenChar; BUFFER_WIDTH * BUFFER_HEIGHT],
}

pub const DEFAULT_COLOR: ColorCode = ColorCode::new(Color::Yellow, Color::Black);

lazy_static! {
    pub static ref WRITER: Mutex<Writer<'static>> =
        Mutex::new(Writer::new(0, DEFAULT_COLOR, unsafe {
            &mut *(0xb8000 as *mut Buffer)
        }));
}

/// A VGA buffer writer
pub struct Writer<'a> {
    pub column_position: usize,
    pub color_code: ColorCode,
    buffer: Volatile<&'a mut [ScreenChar]>,
}

impl<'a> Writer<'a> {
    /// Create a new writer
    pub fn new(
        column_position: usize,
        color_code: ColorCode,
        buffer: &'a mut Buffer,
    ) -> Writer<'a> {
        Writer {
            column_position,
            color_code,
            buffer: Volatile::new(&mut buffer.chars[..]),
        }
    }
    /// Write a byte to the VGA buffer
    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();
                }
                let row = BUFFER_HEIGHT - 1;
                let col = self.column_position;
                let ix = row * BUFFER_WIDTH + col;
                let color_code = self.color_code;
                self.buffer.index_mut(ix).write(ScreenChar {
                    code: byte,
                    color_code,
                });
                self.column_position += 1;
            }
        }
    }
    /// Print a new line on the VGA buffer
    pub fn new_line(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let character = self.buffer.index(row * BUFFER_WIDTH + col).read();
                self.buffer
                    .index_mut((row - 1) * BUFFER_WIDTH + col)
                    .write(character)
            }
        }
        self.clear_row(BUFFER_HEIGHT - 1);
        self.column_position = 0;
    }
    /// Clear a given row
    pub fn clear_row(&mut self, row: usize) {
        if row >= BUFFER_HEIGHT {
            return;
        }
        let blank = ScreenChar {
            code: b' ',
            color_code: self.color_code,
        };
        for col in 0..BUFFER_WIDTH {
            self.buffer.index_mut(row * BUFFER_WIDTH + col).write(blank)
        }
    }
}

impl Write for Writer<'_> {
    /// Write a string to the VGA buffer
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            match byte {
                0x20..=0x7e | b'\n' => self.write_byte(byte),
                _ => self.write_byte(0xfe), //■
            }
        }
        Ok(())
    }
}

/// Print to the VGA buffer
#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga_buffer::_print(format_args!($($arg)*)));
}

/// Print a line to the VGA buffer
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

/// Print a set of arguments to the VGA buffer
#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    #[allow(unused_imports)]
    use core::fmt::Write;
    use x86_64::instructions::interrupts;
    interrupts::without_interrupts(|| {
        WRITER.lock().write_fmt(args).unwrap();
    })
}
