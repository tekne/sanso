/// An exit code
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
#[repr(u32)]
pub enum ExitCode {
    /// Halt, reporting a successful program execution
    Success = 0x10,
    /// Halt, reporting a non-panic failure
    Failure = 0x11,
    /// Halt, reporting a kernel panic
    Panic = 0x12,
}

/// Abort with a given code
pub fn abort(exit_code: ExitCode) -> ! {
    use x86_64::instructions::port::Port;

    unsafe {
        let mut port = Port::new(0xf4);
        port.write(exit_code as u32)
    }

    loop {
        x86_64::instructions::hlt()
    }
}

/// The panic handler for library tests
#[cfg(all(test, target_os = "none"))] // This is a hack so things play nice with RLS\
#[panic_handler]
pub fn panic_handler(info: &core::panic::PanicInfo) -> ! {
    crate::testing::test_panic_handler(info)
}
