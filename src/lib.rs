#![feature(custom_test_frameworks)]
#![feature(abi_x86_interrupt)]
#![feature(wake_trait)]
#![test_runner(crate::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]
#![no_std]
#![cfg_attr(test, no_main)]
#![feature(alloc_error_handler)]

extern crate alloc;

pub mod allocator;
pub mod gdt;
pub mod interrupts;
pub mod memory;
pub mod panic;
pub mod serial;
pub mod task;
pub mod testing;
pub mod vga_buffer;

use alloc::alloc::Layout;
#[allow(unused_imports)]
use bootloader::{entry_point, BootInfo};

/// Initialize the kernel
pub fn init() {
    interrupts::init_idt();
    gdt::init();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();
}

#[cfg(test)]
entry_point!(test_kernel_main);

#[cfg(test)]
pub fn test_kernel_main(_boot_info: &'static BootInfo) -> ! {
    // Initialize the kernel
    init();
    // Begin general test sequence
    test_main();
    loop {}
}

#[alloc_error_handler]
fn alloc_error_handler(layout: Layout) -> ! {
    panic!("Allocation error: {:#?}", layout)
}

#[cfg(test)]
mod tests {
    #[test_case]
    fn test_breakpoint_exception() {
        x86_64::instructions::interrupts::int3()
    }
}
