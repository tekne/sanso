use crate::*;
use alloc::boxed::Box;
use core::{
    borrow::Borrow,
    cmp::Ordering,
    future::Future,
    pin::Pin,
    sync::atomic::{self, AtomicU64},
    task::{Context, Poll},
};

pub mod keyboard;
pub mod simple_executor;
pub mod executor;

/// A task to be executed
pub struct Task {
    id: TaskId,
    future: Pin<Box<dyn Future<Output = ()>>>,
}

impl PartialEq for Task {
    #[inline]
    fn eq(&self, other: &Task) -> bool {
        self.id == other.id
    }
}

impl Eq for Task {}

impl PartialOrd for Task {
    #[inline]
    fn partial_cmp(&self, other: &Task) -> Option<Ordering> {
        self.id.partial_cmp(&other.id)
    }
}

impl Ord for Task {
    #[inline]
    fn cmp(&self, other: &Task) -> Ordering {
        self.id.cmp(&other.id)
    }
}

impl Borrow<TaskId> for Task {
    #[inline]
    fn borrow(&self) -> &TaskId {
        &self.id
    }
}

impl Task {
    pub fn new(future: impl Future<Output = ()> + 'static) -> Task {
        Task {
            id: TaskId::new(),
            future: Box::pin(future),
        }
    }
    pub fn poll(&mut self, context: &mut Context) -> Poll<()> {
        self.future.as_mut().poll(context)
    }
}

/// A task ID
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct TaskId(u64);

impl TaskId {
    fn new() -> Self {
        static NEXT_ID: AtomicU64 = AtomicU64::new(0);
        TaskId(NEXT_ID.fetch_add(1, atomic::Ordering::Relaxed))
    }
}
