use bootloader::bootinfo::{MemoryMap, MemoryRegion, MemoryRegionType};
use x86_64::{
    structures::paging::{
        FrameAllocator, Mapper, OffsetPageTable, Page, PageTable, PageTableFlags, PhysFrame,
        Size4KiB,
    },
    PhysAddr, VirtAddr,
};

/// Get a mutable reference to the active level 4 table
#[allow(unused_unsafe)] // Ironic...
unsafe fn active_level_4_table(physical_memory_offset: VirtAddr) -> &'static mut PageTable {
    use x86_64::registers::control::Cr3;
    let (level_4_table_frame, _) = Cr3::read();
    let phys = level_4_table_frame.start_address();
    let virt = physical_memory_offset + phys.as_u64();
    let page_table_ptr: *mut PageTable = virt.as_mut_ptr();
    unsafe { &mut *page_table_ptr }
}

/// Initialize a new `OffsetPageTable`
pub unsafe fn init(physical_memory_offset: VirtAddr) -> OffsetPageTable<'static> {
    let level_4_table = active_level_4_table(physical_memory_offset);
    OffsetPageTable::new(level_4_table, physical_memory_offset)
}

pub fn create_example_mapping(
    page: Page,
    mapper: &mut OffsetPageTable,
    frame_allocator: &mut impl FrameAllocator<Size4KiB>,
) {
    use PageTableFlags as Flags;
    let frame = PhysFrame::containing_address(PhysAddr::new(0xb8000));
    let flags = Flags::PRESENT | Flags::WRITABLE;
    let map_to_result = unsafe {
        // FIXME: this is bad...
        mapper.map_to(page, frame, flags, frame_allocator)
    };
    map_to_result.expect("map_to failed").flush();
}

/// An empty frame allocator, which always fails
pub struct EmptyFrameAllocator;

unsafe impl FrameAllocator<Size4KiB> for EmptyFrameAllocator {
    fn allocate_frame(&mut self) -> Option<PhysFrame> {
        None
    }
}

pub struct BootInfoFrameAllocator<'a> {
    memory_map: &'a MemoryMap,
    region_no: usize,
    frame_no: u64,
}

impl<'a> BootInfoFrameAllocator<'a> {
    pub unsafe fn init(memory_map: &'a MemoryMap) -> BootInfoFrameAllocator<'a> {
        BootInfoFrameAllocator {
            memory_map,
            region_no: 0,
            frame_no: 0,
        }
    }
    fn get_region(&self) -> Option<&'a MemoryRegion> {
        self.memory_map.get(self.region_no)
    }
    fn fix_region(&mut self) -> Option<&'a MemoryRegion> {
        loop {
            let region = self.get_region()?;
            if region.region_type == MemoryRegionType::Usable {
                return Some(region);
            }
            self.region_no += 1;
            self.frame_no = 0;
        }
    }
}

unsafe impl FrameAllocator<Size4KiB> for BootInfoFrameAllocator<'_> {
    fn allocate_frame(&mut self) -> Option<PhysFrame> {
        loop {
            let region = self.fix_region()?;
            self.frame_no = self.frame_no.max(region.range.start_frame_number);
            if self.frame_no <= region.range.end_frame_number {
                let addr = PhysAddr::new(self.frame_no * 4096);
                let frame = PhysFrame::containing_address(addr);
                self.frame_no += 1;
                return Some(frame);
            }
            self.region_no += 1;
            self.frame_no = 0;
        }
    }
}
