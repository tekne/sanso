use crate::*;
#[allow(unused_imports)]
use core::panic::PanicInfo;
use panic::*;

/// A test
pub trait Testable {
    fn run(&self) -> TestResult;
}

/// The result of a test
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TestResult {
    Success,
    Failure,
}

/// A value which may be used as a test result
pub trait IntoTestResult {
    fn into_result(self) -> TestResult;
}

impl IntoTestResult for () {
    fn into_result(self) -> TestResult {
        serial_println!("[ok]");
        TestResult::Success
    }
}

impl IntoTestResult for TestResult {
    fn into_result(self) -> TestResult {
        match self {
            TestResult::Success => ().into_result(),
            TestResult::Failure => {
                serial_println!("[failed]");
                TestResult::Failure
            }
        }
    }
}

impl<T, R> Testable for T
where
    T: Fn() -> R,
    R: IntoTestResult,
{
    fn run(&self) -> TestResult {
        serial_print!("{}...\t", core::any::type_name::<T>());
        self().into_result()
    }
}

/// The standard panic handler
pub fn test_panic_handler(info: &PanicInfo) -> ! {
    if cfg!(test) {
        serial_println!("[failed] {}", info);
        abort(ExitCode::Panic)
    } else {
        serial_println!("{}", info);
        println!("{}", info);
        loop {
            x86_64::instructions::hlt()
        }
    }
}

/// The (in-kernel) test runner
pub fn test_runner(tests: &[&dyn Testable]) {
    serial_println!("Running {} tests", tests.len());
    let mut failures = 0;
    for test in tests {
        if let TestResult::Failure = test.run() {
            failures += 1
        }
    }
    if failures == 0 {
        serial_println!("All tests passed!");
        abort(ExitCode::Success)
    } else {
        serial_println!("{} tests failed!", failures);
        abort(ExitCode::Failure)
    }
}
