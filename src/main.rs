#![feature(custom_test_frameworks)]
#![test_runner(sanso::testing::test_runner)]
#![reexport_test_harness_main = "test_main"]
#![no_std]
#![no_main]
extern crate alloc;

//use alloc::boxed::Box;
use bootloader::{entry_point, BootInfo};
use sanso::*;
use task::{executor::Executor, keyboard, Task};
use x86_64::VirtAddr;

/// The panic handler for library tests
#[cfg(target_os = "none")] // This is a hack so things play nice with RLS\
#[panic_handler]
pub fn panic_handler(info: &core::panic::PanicInfo) -> ! {
    testing::test_panic_handler(info)
}

entry_point!(kernel_main);

#[no_mangle]
pub fn kernel_main(boot_info: &'static BootInfo) -> ! {
    print!("Initializing kernel...    ");
    // Initialize the kernel
    sanso::init();
    println!("[ok]");

    // Initialize the page table
    print!("Initializing page table...    ");
    let physical_memory_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(physical_memory_offset) };
    let mut frame_allocator =
        unsafe { memory::BootInfoFrameAllocator::init(&boot_info.memory_map) };
    println!("[ok]");

    // Initialize the heap
    print!("Initializing heap...    ");
    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");
    println!("[ok]");

    // Testing the allocator
    print!("Initializing executor...    ");
    let mut executor = Executor::new();
    println!("[ok]");
    
    #[cfg(not(test))]
    executor.spawn(Task::new(keyboard::print_keypresses()));
    
    executor.run()
}
